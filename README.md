# ADSAI '22

This repository contains resources for the poster presented at
[ADSAI '22](https://www.idsai.manchester.ac.uk/connect/events/conference/conference2022/).
The poster is based on the paper titled '*[Exploring Rawlsian Fairness
for K-means Clustering](https://doi.org/10.48550/arxiv.2205.02052)*' by
Stanley Simoes, Deepak P, and Muiris MacCarthaigh, accepted at
[ICDSE '21](https://www.iitp.ac.in/~icdse2021/).

## Resources

1. **[ExtendedAbstract.pdf](ExtendedAbstract.pdf):** extended abstract
2. **[Poster.pdf](Poster.pdf):** poster

## Authors

- **[Stanley Simoes](https://pure.qub.ac.uk/en/persons/stanley-simoes)** (School of Electronics, Electrical Engineering and Computer Science, QUB)
- **[Deepak P](https://pure.qub.ac.uk/en/persons/deepak-padmanabhan)** (School of Electronics, Electrical Engineering and Computer Science, QUB)
- **[Muiris MacCarthaigh](https://pure.qub.ac.uk/en/persons/muiris-maccarthaigh)**  (School of History, Anthropology, Philosophy and Politics, QUB)
